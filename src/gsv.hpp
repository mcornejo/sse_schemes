#pragma once

#include <sse/crypto/prf.hpp>
#include <sse/crypto/set_hash.hpp>
#include <sse/dbparser/DBParser.h>
#include <sse/verifiable_containers/verifiable_map.hpp>

#include <string>
#include <memory>
#include <exception>

namespace sse
{
namespace sks
{


struct set_hash_string
{
    const std::string operator()(const sse::crypto::SetHash& h) const
    {
        return h.hex();
    }
};

class GsvClient
{
public:
	struct AddToken;
	struct DelToken;
	class InvalidHashException;
	
    typedef std::string		keyword_type;
	typedef unsigned		index_type;
	
	typedef sse::crypto::SetHash set_hash_type;
	typedef std::string key_type;
	typedef verifiable_containers::hash_key_gen<key_type> key_hash;
		
		
	typedef sse::verifiable_containers::verifiable_map<key_type, set_hash_type, key_hash, set_hash_string> verifiable_map_type;
	
	
    typedef std::string		keyword_label_type;
    typedef std::string		doc_label_type;
	
	typedef AddToken		add_element_type;
	typedef std::list<add_element_type>
							add_token_type;
	typedef std::list<verifiable_map_type::insertion_proof>
							add_response_type;
	
	typedef DelToken		del_element_type;
	typedef std::list<del_element_type>
							del_token_type;
	typedef std::list<verifiable_map_type::insertion_proof>
							del_response_type;

	
	static constexpr unsigned kDerivedKeysSize = 16;
	
	// 64 bits labels
	static constexpr unsigned kKeywordLabelSize = 8; 
	static constexpr unsigned kDocLabelSize = 8; //sizeof(doc_label_type);
	
	
	GsvClient(sse::dbparser::DBParser &parser);
	~GsvClient();

	std::unique_ptr<verifiable_map_type> finalize_setup();

	
	keyword_label_type keyword_label(const std::string& keyword) const;
	doc_label_type document_label(const std::string& keyword, const index_type& doc) const;
	std::list<doc_label_type> document_labels(const std::string& keyword, const std::list<index_type>& documents) const;
	
	void check_results(const std::string& keyword, const std::list<unsigned> &documents, const verifiable_map_type::lookup_proof_type& vht_proof) const;
	
	add_token_type add_token(const index_type& document, const std::list<keyword_type>& keywords) const;
	add_element_type gen_add_token_element(const index_type& document, const keyword_type& keyword) const;
	void check_add(const add_token_type& add_token, add_response_type& insertion_proofs);


	del_token_type delete_token(const index_type& document, const std::list<keyword_type>& keywords) const;
	del_element_type gen_del_token_element(const index_type& document, const keyword_type& keyword) const;
	void check_del(const del_token_type& del_token, del_response_type& insertion_proof);
		
private:
	void hash_list(const std::string& keyword, const std::list<unsigned> &documents);
	
	
	bool is_setup_;
	
	
	verifiable_map_type *vht_;
	verifiable_map_type::digest_type digest_;

	sse::crypto::Prf<kKeywordLabelSize> keyword_prf_;
	sse::crypto::Prf<kDerivedKeysSize> derivation_prf_;
	
};

struct GsvClient::AddToken
{
	GsvClient::keyword_label_type keyword_label;
	GsvClient::set_hash_type	set_hash;
};

struct GsvClient::DelToken
{
	GsvClient::keyword_label_type keyword_label;
	GsvClient::set_hash_type	set_hash;
};

class GsvClient::InvalidHashException : public std::exception
{
public:
	InvalidHashException();
	virtual const char* what() const throw();
};

class GsvServer
{
public:
	typedef GsvClient::verifiable_map_type 	verifiable_map_type;
    typedef GsvClient::keyword_label_type	keyword_label_type;
	
	GsvServer(std::unique_ptr<verifiable_map_type>&& vht);
	~GsvServer();
	
	verifiable_map_type::lookup_proof_type get_proof(const keyword_label_type& label) const;
	
	GsvClient::add_response_type add(const GsvClient::add_token_type& add_token);
	GsvClient::del_response_type del(const GsvClient::del_token_type& del_token);
	
private:
	
	std::unique_ptr<verifiable_map_type> vht_;
};




}
}
#include "gsv.hpp"

#include <iostream>

namespace sse
{
namespace sks
{

GsvClient::GsvClient(sse::dbparser::DBParser &parser):
is_setup_(false), vht_(new verifiable_map_type()), keyword_prf_(), derivation_prf_()
{
    parser.addCallbackList(std::bind(&GsvClient::hash_list,this,std::placeholders::_1, std::placeholders::_2));

}

GsvClient::~GsvClient()
{
	
}

void GsvClient::hash_list(const std::string& keyword, const std::list<unsigned> &documents)
{
	assert(!is_setup_);
		
	set_hash_type set_hash;
		
	auto labels = document_labels(keyword, documents);
	for(auto it = labels.begin(); it != labels.end(); ++it)
	{
		set_hash.add_element(*it);
	}

    vht_->lazy_insert(keyword_label(keyword), set_hash);
}


std::unique_ptr<GsvClient::verifiable_map_type> GsvClient::finalize_setup()
{
	is_setup_ = true;
	vht_->refresh_digest();
	digest_ = vht_->digest();
	return std::unique_ptr<verifiable_map_type>(vht_);
}


GsvClient::keyword_label_type GsvClient::keyword_label(const std::string& keyword) const
{
	return keyword_prf_.prf_string(keyword);
}

GsvClient::doc_label_type GsvClient::document_label(const std::string& keyword, const index_type& doc) const
{
	crypto::Prf<kDocLabelSize> label_prf(derivation_prf_.prf_string(keyword));
	std::array<uint8_t, kDocLabelSize> doc_label_array = label_prf.prf((unsigned char*)&doc, sizeof(index_type));
	return std::string(doc_label_array.begin(), doc_label_array.end());
}

std::list<GsvClient::doc_label_type> GsvClient::document_labels(const std::string& keyword, const std::list<index_type>& documents) const
{
	crypto::Prf<kDocLabelSize> label_prf(derivation_prf_.prf_string(keyword));
	std::array<uint8_t, kDocLabelSize> doc_label_array;
	std::list<GsvClient::doc_label_type> l;
	
	for(auto doc = documents.begin(); doc != documents.end(); ++doc)
	{
   		doc_label_array = label_prf.prf((unsigned char*)&(*doc), sizeof(index_type));
		l.push_back(std::string(doc_label_array.begin(), doc_label_array.end()));
	}
	
	
	return l;
}

void GsvClient::check_results(const std::string& keyword, const std::list<unsigned> &documents, const GsvClient::verifiable_map_type::lookup_proof_type& vht_proof) const
{
	assert(is_setup_);
	
	set_hash_type set_hash;
		
	auto labels = document_labels(keyword, documents);
	for(auto it = labels.begin(); it != labels.end(); ++it)
	{
		set_hash.add_element(*it);
	}
	
	
	// check that the set hash computed from the list is the same as the mapped element.
	if(set_hash == *(vht_proof.value))
	{
		verifiable_map_type::check_lookup_proof_except(digest_, keyword_label(keyword), vht_proof);
	}else{
		// raise an exception
		throw InvalidHashException();
	}
}

GsvClient::add_token_type GsvClient::add_token(const GsvClient::index_type& document, const std::list<keyword_type>& keywords) const
{
	assert(is_setup_);
	
	std::list<add_element_type> token;
	
	for(auto it = keywords.begin(); it != keywords.end(); ++it)
	{
		token.push_back(gen_add_token_element(document, *it));
	}
	
	auto comp = [](const add_element_type& x, const add_element_type& y)
	{
		return (x.keyword_label) < (y.keyword_label);
	};
	token.sort(comp);

	return token;
}

GsvClient::add_element_type GsvClient::gen_add_token_element(const GsvClient::index_type& document, const GsvClient::keyword_type& keyword) const
{
	add_element_type add_elt;
	
	
	std::string doc_label = document_label(keyword, document);

	add_elt.keyword_label = keyword_label(keyword);
	add_elt.set_hash.add_element(doc_label);
	
	return add_elt;
}

void GsvClient::check_add(const GsvClient::add_token_type& add_token, add_response_type& insertion_proofs)
{
	assert(add_token.size() == insertion_proofs.size());

	
	auto token_it = add_token.begin();
	auto proof_it = insertion_proofs.begin();

	auto local_digest = digest_;
	
	for( ; token_it != add_token.end() && proof_it != insertion_proofs.end(); ++token_it, ++proof_it)
	{
		verifiable_map_type::check_update_addition(local_digest, token_it->keyword_label, proof_it->value, *proof_it);
				
	}
	digest_ = local_digest;	
}


GsvClient::del_token_type GsvClient::delete_token(const GsvClient::index_type& document, const std::list<keyword_type>& keywords) const
{
	assert(is_setup_);
	
	std::list<del_element_type> token;
	
	for(auto it = keywords.begin(); it != keywords.end(); ++it)
	{
		token.push_back(gen_del_token_element(document, *it));
	}
	
	auto comp = [](const del_element_type& x, const del_element_type& y)
	{
		return (x.keyword_label) < (y.keyword_label);
	};
	token.sort(comp);

	return token;
}

GsvClient::del_element_type GsvClient::gen_del_token_element(const GsvClient::index_type& document, const GsvClient::keyword_type& keyword) const
{
	del_element_type del_elt;

	
	std::string doc_label = document_label(keyword, document);

	del_elt.keyword_label = keyword_label(keyword);
	del_elt.set_hash.add_element(doc_label);
	
	return del_elt;
}

void GsvClient::check_del(const del_token_type& del_token, del_response_type& insertion_proofs)
{
	assert(del_token.size() == insertion_proofs.size());

	
	auto token_it = del_token.begin();
	auto proof_it = insertion_proofs.begin();

	auto local_digest = digest_;
	
	for( ; token_it != del_token.end() && proof_it != insertion_proofs.end(); ++token_it, ++proof_it)
	{
		verifiable_map_type::check_update_addition(local_digest, token_it->keyword_label, proof_it->value, *proof_it);
		
	}
	
	digest_ = local_digest;	
}

	
GsvClient::InvalidHashException::InvalidHashException()
{
}

const char* GsvClient::InvalidHashException::what() const throw()
{
	std::string message = "Invalid set hashes: client's and server's hashes do not match.";
	return message.c_str();
}
	
GsvServer::GsvServer(std::unique_ptr<verifiable_map_type>&& vht) :
vht_(std::move(vht))
{
}

GsvServer::~GsvServer()
{
}


GsvServer::verifiable_map_type::lookup_proof_type GsvServer::get_proof(const keyword_label_type& label) const
{
	return vht_->get(label);
}


GsvClient::add_response_type GsvServer::add(const GsvClient::add_token_type& add_token)
{
	std::list<verifiable_map_type::insertion_proof> proofs;
		
	for(auto it = add_token.begin(); it != add_token.end(); ++it)
	{
		GsvClient::set_hash_type set_hash;
		vht_->retrieve(it->keyword_label, set_hash);
		 
		set_hash.add_set(it->set_hash);
				
		proofs.push_back(vht_->set(it->keyword_label, set_hash)); 
	}
		
	return proofs;
}

GsvClient::del_response_type GsvServer::del(const GsvClient::del_token_type& del_token)
{
	std::list<verifiable_map_type::insertion_proof> proofs;
		
	for(auto it = del_token.begin(); it != del_token.end(); ++it)
	{
		GsvClient::set_hash_type set_hash;
		vht_->retrieve(it->keyword_label, set_hash);
		 
		set_hash.remove_set(it->set_hash);
				
		proofs.push_back(vht_->set(it->keyword_label, set_hash)); 
	}
		
	return proofs;
}

}
}
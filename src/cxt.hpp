#pragma once

#include "./simple_sks.hpp"
#include "./gsv.hpp"
#include "./cross_tags.hpp"

#include <sse/dbparser/DBParser.h>

#include <memory>

namespace sse
{

namespace sks
{

class CxtClient
{
public:
	
	struct Edb;
	struct SearchToken;
	struct AddToken;
	struct AddResponse;
	struct DelToken;
	struct DelResponse;
	
	struct IntermediateResult;
	
	typedef SimpleSksClient::keyword_type 	keyword_type;
	typedef SimpleSksClient::index_type 	index_type;
	
	typedef Edb				encrypted_database_type;
	typedef SearchToken		sk_search_token_type;
	typedef AddToken		add_token_type;
	typedef AddResponse		add_response_type;
	typedef DelToken		del_token_type;
	typedef DelResponse		del_response_type;

	typedef CrossTagClient::filter_token_type xt_search_token_type;
	
	typedef IntermediateResult intermediate_result_type;
	typedef CrossTagClient::response_type filters_type;
	
	CxtClient(sse::dbparser::DBParser &parser);
	~CxtClient();
	
	encrypted_database_type finalize_setup();
	
	sk_search_token_type single_keyword_search(const keyword_type& keyword) const;
	std::list<index_type> decrypt_check_intermediate_results(const keyword_type& keyword, const intermediate_result_type& intermediate_results) const;
	
	xt_search_token_type cross_tags(const std::list<index_type>& intermediate_results, const std::list<keyword_type>& keywords) const;
	
	std::list<index_type> filter_results(const std::list<index_type>& indices, const std::list<keyword_type>& keywords, const filters_type& filters) const;
	
	
	add_token_type add_token(const index_type& document, const std::list<keyword_type>& keywords);
	void check_update_add(const add_token_type& add_tokens, add_response_type& response);


	del_token_type delete_token(const index_type& document, const std::list<keyword_type>& keywords) const;
	void check_update_del(const del_token_type& del_tokens, del_response_type& response);
		
private:
	SimpleSksClient 	sks_client_;
	GsvClient 			sks_verifier_;
	CrossTagClient 		cross_tag_client_;
	
	std::list<keyword_type> sks_add_tmp_state_;
};

struct CxtClient::Edb
{
	std::unique_ptr<SimpleSksClient::encrypted_database_type> 	sks_edb;
	std::unique_ptr<GsvClient::verifiable_map_type> 			gsv_edb;
	std::unique_ptr<CrossTagClient::verifiable_set_type> 		xt_edb;
};

struct CxtClient::SearchToken
{
	SimpleSksClient::search_token_type 	sks_token;
	GsvClient::keyword_label_type 		gsv_token;
};

struct CxtClient::AddToken
{
	SimpleSksClient::add_token_type 	sks_token;
	GsvClient::add_token_type 			gsv_token;
	CrossTagClient::add_token_type 		xt_token;
};


struct CxtClient::AddResponse
{
	SimpleSksClient::add_response_type 		sks_response;
	GsvClient::add_response_type 			gsv_response;
	CrossTagClient::add_response_type 		xt_response;
};

struct CxtClient::DelToken
{
	SimpleSksClient::del_token_type 	sks_token;
	GsvClient::del_token_type 			gsv_token;
	CrossTagClient::del_token_type	 	xt_token;
};

struct CxtClient::DelResponse
{
	GsvClient::del_response_type 			gsv_response;
	CrossTagClient::del_response_type 		xt_response;
};


struct CxtClient::IntermediateResult
{
	std::list<CxtClient::index_type> 					indices;
	GsvClient::verifiable_map_type::lookup_proof_type 	proof;
};

class CxtServer
{
public:
	
	CxtServer(CxtClient::Edb &&edb);
	~CxtServer();
	
	CxtClient::intermediate_result_type keyword_search(const CxtClient::sk_search_token_type& token) const;

	CxtClient::filters_type get_filters(const CxtClient::xt_search_token_type& cross_token) const;
	
	CxtClient::add_response_type add(const CxtClient::add_token_type& add_token);

	CxtClient::del_response_type del(const CxtClient::del_token_type& del_token);
	
private:
	SimpleSksServer 	sks_server_;
	GsvServer 			sks_prover_;
	CrossTagServer 		cross_tag_server_;
};

}
}
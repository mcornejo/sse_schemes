#include "cross_tags.hpp"

namespace sse
{
namespace sks
{


CrossTagClient::CrossTagClient(sse::dbparser::DBParser &parser):
is_setup_(false), e_set_(new verifiable_set_type()), tag_prf_()
{
    parser.addCallbackPair(std::bind(&CrossTagClient::encrypt_pair,this,std::placeholders::_1, std::placeholders::_2));

}

CrossTagClient::~CrossTagClient()
{
	
}


void CrossTagClient::encrypt_pair(const std::string& keyword, const unsigned &doc)
{
	assert(!is_setup_);
		
	e_set_->lazy_insert(cross_tag(doc, keyword));
}

std::unique_ptr<CrossTagClient::verifiable_set_type> CrossTagClient::finalize_setup()
{
	is_setup_ = true;
	e_set_->refresh_digest();
	digest_ = e_set_->digest();
	return std::unique_ptr<verifiable_set_type>(e_set_);
}

CrossTagClient::cross_tag_type CrossTagClient::cross_tag(const CrossTagClient::index_type& index, const CrossTagClient::keyword_type& keyword) const
{
	std::string input((char*)&index, sizeof(index_type));
	input.append(keyword);
	
	return tag_prf_.prf_string(input);
}

std::list<CrossTagClient::cross_tag_type> CrossTagClient::cross_tag_list(const CrossTagClient::index_type& index, const std::list<CrossTagClient::keyword_type>& keywords) const
{
	std::list<cross_tag_type> tags;
	
	for(auto it = keywords.begin(); it != keywords.end(); ++it)
	{
		tags.push_back(cross_tag(index, *it));
	}
	
	return tags;
}

CrossTagClient::filter_token_type CrossTagClient::filter_tokens(const std::list<CrossTagClient::index_type>& indices, const std::list<CrossTagClient::keyword_type>& keywords) const
{
	filter_token_type token;
	for(auto it_indices = indices.begin(); it_indices != indices.end(); ++it_indices)
	{
		token.push_back(cross_tag_list(*it_indices, keywords));
	}
	return token;
}

	
void CrossTagClient::check_results(const std::string& keyword, const CrossTagClient::index_type& index, const CrossTagClient::verifiable_set_type::membership_proof_type& proof) const
{
	auto tag = cross_tag(index, keyword);
	
	verifiable_set_type::check_membership_proof_except(digest_, tag, proof);
}


bool CrossTagClient::match(const std::list<CrossTagClient::verifiable_set_type::membership_proof_type>& proofs) const
{
	for(auto it = proofs.begin(); it != proofs.end(); ++it)
	{
		if(!it->found)
		{
			return false;
		}
	}
	return true;
}

std::list<CrossTagClient::index_type> CrossTagClient::filter_results(const std::list<CrossTagClient::index_type>& indices, const std::list<CrossTagClient::keyword_type>& keywords, const CrossTagClient::response_type& response) const
{
	assert(indices.size() == response.size());

	std::list<CrossTagClient::index_type> results;
	
	auto it_indices = indices.begin();
	auto it_response = response.begin();
	
	for(; it_indices != indices.end(); ++it_indices, ++it_response)
	{
		assert(it_response->size() == keywords.size());
		
		auto it_keywords = keywords.begin();
		auto it_proofs = it_response->begin();
		
		for(; it_keywords != keywords.end(); ++it_keywords, ++it_proofs)
		{
			check_results(*it_keywords, *it_indices, *it_proofs);
		}
		if(match(*it_response))
		{
			results.push_back(*it_indices);
		}
	}
	return results;
}

CrossTagClient::add_token_type CrossTagClient::add_token(const CrossTagClient::index_type& document, const std::list<CrossTagClient::keyword_type>& keywords) const
{
	add_token_type token;
	
	for(auto it = keywords.begin(); it != keywords.end(); ++it)
	{
		token.push_back(cross_tag(document, *it));
	}
	
	return token;
}

void CrossTagClient::check_add(const CrossTagClient::add_token_type& add_token, add_response_type& insertion_proofs)
{
	assert(add_token.size() == insertion_proofs.size());
	
	auto token_it = add_token.begin();
	auto proof_it = insertion_proofs.begin();
	
	auto local_digest = digest_;
	
	for( ; token_it != add_token.end() && proof_it != insertion_proofs.end(); ++token_it, ++proof_it)
	{
		local_digest = verifiable_set_type::check_update_addition(local_digest, *token_it, *proof_it);
	}
	
	digest_ = local_digest;	
}


CrossTagClient::del_token_type CrossTagClient::delete_token(const CrossTagClient::index_type& document, const std::list<CrossTagClient::keyword_type>& keywords) const
{
	del_token_type token;
	
	for(auto it = keywords.begin(); it != keywords.end(); ++it)
	{
		token.push_back(cross_tag(document, *it));
	}
	
	return token;
}

void CrossTagClient::check_del(const CrossTagClient::del_token_type& del_token, del_response_type& deletion_proofs)
{
	assert(del_token.size() == deletion_proofs.size());
	
	auto token_it = del_token.begin();
	auto proof_it = deletion_proofs.begin();
	
	auto local_digest = digest_;
	
	for( ; token_it != del_token.end() && proof_it != deletion_proofs.end(); ++token_it, ++proof_it)
	{
		local_digest = verifiable_set_type::check_update_deletion(local_digest, *token_it, *proof_it);
	}
	
	digest_ = local_digest;	
}




CrossTagServer::CrossTagServer(std::unique_ptr<CrossTagServer::verifiable_set_type> &&e_set) : 
e_set_(std::move(e_set))
{
	
}

CrossTagServer::~CrossTagServer()
{
	
}

CrossTagClient::response_type CrossTagServer::retrieve(const CrossTagClient::filter_token_type& filter_tokens) const
{
	CrossTagClient::response_type response;
	
	for(auto filter = filter_tokens.begin(); filter != filter_tokens.end(); ++filter)
	{
		std::list<verifiable_set_type::membership_proof_type> r;
		
		for(auto it = filter->begin(); it != filter->end(); ++it)
		{
			r.push_back(e_set_->contains(*it));
		}
		
		response.push_back(std::move(r));
	}
	
	return response;
}

CrossTagClient::add_response_type CrossTagServer::add(const CrossTagClient::add_token_type& add_token)
{
	std::list<CrossTagServer::verifiable_set_type::insertion_proof> proofs;
	
	for(auto it = add_token.begin(); it != add_token.end(); ++it)
	{
		proofs.push_back(e_set_->add(*it));
	}
	
	return proofs;
}

CrossTagClient::del_response_type CrossTagServer::del(const CrossTagClient::del_token_type& del_token)
{
	std::list<CrossTagServer::verifiable_set_type::deletion_proof> proofs;
	
	for(auto it = del_token.begin(); it != del_token.end(); ++it)
	{
		proofs.push_back(e_set_->remove(*it));
	}
	
	return proofs;
}


}
}